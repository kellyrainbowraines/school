package School.src.school;

import javax.swing.JOptionPane;

public class Main {

	public static void main(String[] args) {
		boolean inputStudent = false;
		boolean inputTeacher = false;
		//start
		JOptionPane.showMessageDialog(null, "Hello!");
		String input = JOptionPane.showInputDialog("Are you a Student or Teacher? Type S for Student or T for Teacher.");
		//student option
		if (input.equals("S") || input.equals("s") ||  input.equals("Student") || input.equals("student")) {
			inputStudent = true;
			Student student = new Student();
			student.name = JOptionPane.showInputDialog("What is your name?");
			while (student.name.equals("") || student.name.matches(".*\\d.*")) {
				JOptionPane.showMessageDialog(null, "Please enter your name.");
				student.name = JOptionPane.showInputDialog("What is your name?");
			}
			student.gradYear = JOptionPane.showInputDialog("What year will you graduate?");
			while (student.gradYear.equals("") || !student.gradYear.matches(".*\\d.*") || student.gradYear.length() > 4 || student.gradYear.length() < 4) {
				JOptionPane.showMessageDialog(null, "Please enter the a four-digit year that you will graduate.");
				student.gradYear = JOptionPane.showInputDialog("What year will you graduate?");
			}
			String tempGPA = JOptionPane.showInputDialog("What is your GPA?");
			while (tempGPA.equals("") || !tempGPA.matches(".*\\d.*")) {
				JOptionPane.showMessageDialog(null, "Please enter your GPA.");
				tempGPA = JOptionPane.showInputDialog("What is your GPA?");
			}
			student.GPA = Double.parseDouble(tempGPA);
			student.studentID = JOptionPane.showInputDialog("What is your Student ID?");
			while (student.studentID.equals("") || !student.studentID.startsWith("S") || student.studentID.length() > 9 || student.studentID.length() < 9) {
				JOptionPane.showMessageDialog(null, "Please enter your Student ID starting with a capital S.");
				student.studentID = JOptionPane.showInputDialog("What is your Student ID?");
			}
			//option for student to calculate current GPA
			String toCalculateGPA = JOptionPane.showInputDialog("Would you like to calculate your current GPA? Type Y for Yes or N for No.");
			while (toCalculateGPA.equals("")) {
				JOptionPane.showMessageDialog(null, "Please enter Yes or No if you would like to calculate your current GPA.");
				toCalculateGPA = JOptionPane.showInputDialog("Would you like to calculate your current GPA? Type Y for Yes or N for No.");
			}
			if (toCalculateGPA.equals("Y") || toCalculateGPA.equals("y") || toCalculateGPA.equals("Yes") || toCalculateGPA.equals("yes")) {
				String As = JOptionPane.showInputDialog("How many A's do you have?");
				while (As.equals("") || As.length() > 7 || As.length() < 0 || !As.matches(".*\\d.*")) {
					JOptionPane.showMessageDialog(null, "Please enter a valid number between 0 and 7.");
					As = JOptionPane.showInputDialog("How many A's do you have?");
				}
				String Bs = JOptionPane.showInputDialog("How many B's do you have?");
				while (Bs.equals("") || Bs.length() > 7 || Bs.length() < 0 || !Bs.matches(".*\\d.*")) {
					JOptionPane.showMessageDialog(null, "Please enter a valid number between 0 and 7.");
					Bs = JOptionPane.showInputDialog("How many B's do you have?");
				}
				String Cs = JOptionPane.showInputDialog("How many C's do you have?");
				while (Cs.equals("") || Cs.length() > 7 || Cs.length() < 0 || !Cs.matches(".*\\d.*")) {
					JOptionPane.showMessageDialog(null, "Please enter a valid number between 0 and 7.");
					Cs = JOptionPane.showInputDialog("How many C's do you have?");
				}
				String Ds = JOptionPane.showInputDialog("How many D's do you have?");
				while (Ds.equals("") || Ds.length() > 7 || Ds.length() < 0 || !Ds.matches(".*\\d.*")) {
					JOptionPane.showMessageDialog(null, "Please enter a valid number between 0 and 7.");
					Ds = JOptionPane.showInputDialog("How many D's do you have?");
				}
				double numOfAs = Double.parseDouble(As);
				double numOfBs = Double.parseDouble(Bs);
				double numOfCs = Double.parseDouble(Cs);
				double numOfDs = Double.parseDouble(Ds);

				double totalnums = numOfAs + numOfBs + numOfCs + numOfDs;
				//loop to check if correct number of classes were entered
				while (totalnums > 7 || totalnums < 7) {
					JOptionPane.showMessageDialog(null, "Make sure to include the correct number of classes for this semester.");
					As = JOptionPane.showInputDialog("How many A's do you have?");
				while (As.equals("") || As.length() > 7 || As.length() < 0 || !As.matches(".*\\d.*")) {
					JOptionPane.showMessageDialog(null, "Please enter a valid number between 0 and 7.");
					As = JOptionPane.showInputDialog("How many A's do you have?");
				}
				Bs = JOptionPane.showInputDialog("How many B's do you have?");
				while (Bs.equals("") || Bs.length() > 7 || Bs.length() < 0 || !Bs.matches(".*\\d.*")) {
					JOptionPane.showMessageDialog(null, "Please enter a valid number between 0 and 7.");
					Bs = JOptionPane.showInputDialog("How many B's do you have?");
				}
				Cs = JOptionPane.showInputDialog("How many C's do you have?");
				while (Cs.equals("") || Cs.length() > 7 || Cs.length() < 0 || !Cs.matches(".*\\d.*")) {
					JOptionPane.showMessageDialog(null, "Please enter a valid number between 0 and 7.");
					Cs = JOptionPane.showInputDialog("How many C's do you have?");
				}
				Ds = JOptionPane.showInputDialog("How many D's do you have?");
				while (Ds.equals("") || Ds.length() > 7 || Ds.length() < 0 || !Ds.matches(".*\\d.*")) {
					JOptionPane.showMessageDialog(null, "Please enter a valid number between 0 and 7.");
					Ds = JOptionPane.showInputDialog("How many D's do you have?");
				}
			}
				double calculatedAs = 0.67 * numOfAs;
				double calculatedBs = 0.5 * numOfBs;
				double calculatedCs = 0.33 * numOfCs;
				double calculatedDs = 0.25 * numOfDs;

				double currentGPA = calculatedAs + calculatedBs + calculatedCs + calculatedDs;
				double finalGPA = (student.GPA + currentGPA)/2;
				student.GPA = finalGPA;

				JOptionPane.showMessageDialog(null, finalGPA);
			}
		//store student's information and return one long string
			String studentInfoName = "Your name is " + student.name;
			String studentInfoGrad = "You're graduating in " + student.gradYear;
			String studentInfoGPA = "Your GPA is " + student.GPA;
			String studentInfoID = "Your student ID is " + student.studentID;
			String studentInfo = studentInfoName + ". " + studentInfoGrad + ". " + studentInfoGPA + ". " + studentInfoID + ".";
			
			JOptionPane.showMessageDialog(null, studentInfo);
			JOptionPane.showMessageDialog(null, "Goodbye, have a nice day!");
		}
		//teacher option
		if (input.equals("T")|| input.equals("t") || input.equals("Teacher") || input.equals("teacher")) {
			inputTeacher = true;
			Teacher teacher = new Teacher();
			teacher.name = JOptionPane.showInputDialog("What is you name?");
			while (teacher.name.equals("") || teacher.name.matches(".*\\d.*")) {
				JOptionPane.showMessageDialog(null, "Please enter a name.");
				teacher.name = JOptionPane.showInputDialog("What is you name?");
			}
			teacher.yearsTeaching = JOptionPane.showInputDialog("How many years have you been teaching?");
			while (teacher.yearsTeaching.equals("") || !teacher.yearsTeaching.matches(".*\\d.*")) {
				JOptionPane.showMessageDialog(null, "Please enter the years you have been teaching.");
				teacher.yearsTeaching = JOptionPane.showInputDialog("How many years have you been teaching?");
			}
			teacher.className = JOptionPane.showInputDialog("What class do you teach?");
			while (teacher.className.equals("") || teacher.className.matches(".*\\d.*")) {
				JOptionPane.showMessageDialog(null, "Please enter the name of the class you teach.");
				teacher.className = JOptionPane.showInputDialog("What class do you teach?");
			}
			teacher.studentsInClass = JOptionPane.showInputDialog("How many students are in your class?");
			while (teacher.studentsInClass.equals("") || !teacher.studentsInClass.matches(".*\\d.*")) {
				JOptionPane.showMessageDialog(null, "Please enter the number of students in your class.");
				teacher.className = JOptionPane.showInputDialog("What class do you teach?");
			}
			teacher.periodClass = JOptionPane.showInputDialog("What period do you teach this class?");
			while (teacher.periodClass.equals("") || !teacher.periodClass.matches(".*\\d.*")) {
				JOptionPane.showMessageDialog(null, "Please enter the period class that you teach.");
				teacher.periodClass = JOptionPane.showInputDialog("What period do you teach this class?");
			}
			String teacherInfoName = "Your name is " + teacher.name;
			String teacherInfoYearsTeaching = "You've been teaching for " + teacher.yearsTeaching + " years. ";
			String teacherInfoClassName = "You teach " + teacher.className;
			String teacherInfoStudents = "You have " + teacher.studentsInClass + " students in your class. ";
			String teacherInfoPeriod = "You teach during " + teacher.periodClass + " hour.";
			String teacherInfo = teacherInfoName + ". " + teacherInfoYearsTeaching + teacherInfoClassName + ". " + teacherInfoStudents + teacherInfoPeriod;
			JOptionPane.showMessageDialog(null, teacherInfo);
			JOptionPane.showMessageDialog(null, "Goodbye, have a nice day!");
		}
		while (!inputStudent && !inputTeacher) {
			JOptionPane.showMessageDialog(null, "Please enter whether you are a student or a teacher.");
			input = JOptionPane.showInputDialog("Are you a Student or Teacher? Type S for Student or T for Teacher.");
		//student option
		if (input.equals("S") || input.equals("s") ||  input.equals("Student") || input.equals("student")) {
			inputStudent = true;
			Student student = new Student();
			student.name = JOptionPane.showInputDialog("What is your name?");
			while (student.name.equals("") || student.name.matches(".*\\d.*")) {
				JOptionPane.showMessageDialog(null, "Please enter your name.");
				student.name = JOptionPane.showInputDialog("What is your name?");
			}
			student.gradYear = JOptionPane.showInputDialog("What year will you graduate?");
			while (student.gradYear.equals("") || !student.gradYear.matches(".*\\d.*") || student.gradYear.length() > 4 || student.gradYear.length() < 4) {
				JOptionPane.showMessageDialog(null, "Please enter the a four-digit year that you will graduate.");
				student.gradYear = JOptionPane.showInputDialog("What year will you graduate?");
			}
			String tempGPA = JOptionPane.showInputDialog("What is your GPA?");
			while (tempGPA.equals("") || !tempGPA.matches(".*\\d.*")) {
				JOptionPane.showMessageDialog(null, "Please enter your GPA.");
				tempGPA = JOptionPane.showInputDialog("What is your GPA?");
			}
			student.GPA = Double.parseDouble(tempGPA);
			student.studentID = JOptionPane.showInputDialog("What is your Student ID?");
			while (student.studentID.equals("") || !student.studentID.startsWith("S") || student.studentID.length() > 9 || student.studentID.length() < 9) {
				JOptionPane.showMessageDialog(null, "Please enter your Student ID starting with a capital S.");
				student.studentID = JOptionPane.showInputDialog("What is your Student ID?");
			}
			//option for student to calculate current GPA
			String toCalculateGPA = JOptionPane.showInputDialog("Would you like to calculate your current GPA? Type Y for Yes or N for No.");
			while (toCalculateGPA.equals("")) {
				JOptionPane.showMessageDialog(null, "Please enter Yes or No if you would like to calculate your current GPA.");
				toCalculateGPA = JOptionPane.showInputDialog("Would you like to calculate your current GPA? Type Y for Yes or N for No.");
			}
			if (toCalculateGPA.equals("Y") || toCalculateGPA.equals("y") || toCalculateGPA.equals("Yes") || toCalculateGPA.equals("yes")) {
				String As = JOptionPane.showInputDialog("How many A's do you have?");
				while (As.equals("") || As.length() > 7 || As.length() < 0 || !As.matches(".*\\d.*")) {
					JOptionPane.showMessageDialog(null, "Please enter a valid number between 0 and 7.");
					As = JOptionPane.showInputDialog("How many A's do you have?");
				}
				String Bs = JOptionPane.showInputDialog("How many B's do you have?");
				while (Bs.equals("") || Bs.length() > 7 || Bs.length() < 0 || !Bs.matches(".*\\d.*")) {
					JOptionPane.showMessageDialog(null, "Please enter a valid number between 0 and 7.");
					Bs = JOptionPane.showInputDialog("How many B's do you have?");
				}
				String Cs = JOptionPane.showInputDialog("How many C's do you have?");
				while (Cs.equals("") || Cs.length() > 7 || Cs.length() < 0 || !Cs.matches(".*\\d.*")) {
					JOptionPane.showMessageDialog(null, "Please enter a valid number between 0 and 7.");
					Cs = JOptionPane.showInputDialog("How many C's do you have?");
				}
				String Ds = JOptionPane.showInputDialog("How many D's do you have?");
				while (Ds.equals("") || Ds.length() > 7 || Ds.length() < 0 || !Ds.matches(".*\\d.*")) {
					JOptionPane.showMessageDialog(null, "Please enter a valid number between 0 and 7.");
					Ds = JOptionPane.showInputDialog("How many D's do you have?");
				}
				double numOfAs = Double.parseDouble(As);
				double numOfBs = Double.parseDouble(Bs);
				double numOfCs = Double.parseDouble(Cs);
				double numOfDs = Double.parseDouble(Ds);

				double totalnums = numOfAs + numOfBs + numOfCs + numOfDs;
				//loop to check if correct number of classes were entered
				if (totalnums > 7 || totalnums < 7) {
					JOptionPane.showMessageDialog(null, "Make sure to include the correct number of classes for this semester.");
					As = JOptionPane.showInputDialog("How many A's do you have?");
				while (As.equals("") || As.length() > 7 || As.length() < 0 || !As.matches(".*\\d.*")) {
					JOptionPane.showMessageDialog(null, "Please enter a valid number between 0 and 7.");
					As = JOptionPane.showInputDialog("How many A's do you have?");
				}
				Bs = JOptionPane.showInputDialog("How many B's do you have?");
				while (Bs.equals("") || Bs.length() > 7 || Bs.length() < 0 || !Bs.matches(".*\\d.*")) {
					JOptionPane.showMessageDialog(null, "Please enter a valid number between 0 and 7.");
					Bs = JOptionPane.showInputDialog("How many B's do you have?");
				}
				Cs = JOptionPane.showInputDialog("How many C's do you have?");
				while (Cs.equals("") || Cs.length() > 7 || Cs.length() < 0 || !Cs.matches(".*\\d.*")) {
					JOptionPane.showMessageDialog(null, "Please enter a valid number between 0 and 7.");
					Cs = JOptionPane.showInputDialog("How many C's do you have?");
				}
				Ds = JOptionPane.showInputDialog("How many D's do you have?");
				while (Ds.equals("") || Ds.length() > 7 || Ds.length() < 0 || !Ds.matches(".*\\d.*")) {
					JOptionPane.showMessageDialog(null, "Please enter a valid number between 0 and 7.");
					Ds = JOptionPane.showInputDialog("How many D's do you have?");
				}
			}
				double calculatedAs = 0.67 * numOfAs;
				double calculatedBs = 0.5 * numOfBs;
				double calculatedCs = 0.33 * numOfCs;
				double calculatedDs = 0.25 * numOfDs;

				double currentGPA = calculatedAs + calculatedBs + calculatedCs + calculatedDs;
				double finalGPA = (student.GPA + currentGPA)/2;
				student.GPA = finalGPA;

				JOptionPane.showMessageDialog(null, finalGPA);
			}
		//store student's information and return one long string
			String studentInfoName = "Your name is " + student.name;
			String studentInfoGrad = "You're graduating in " + student.gradYear;
			String studentInfoGPA = "Your GPA is " + student.GPA;
			String studentInfoID = "Your student ID is " + student.studentID;
			String studentInfo = studentInfoName + ". " + studentInfoGrad + ". " + studentInfoGPA + ". " + studentInfoID + ".";
			
			JOptionPane.showMessageDialog(null, studentInfo);
			JOptionPane.showMessageDialog(null, "Goodbye, have a nice day!");
		}
		//teacher option
		if (input.equals("T")|| input.equals("t") || input.equals("Teacher") || input.equals("teacher")) {
			inputTeacher = true;
			Teacher teacher = new Teacher();
			teacher.name = JOptionPane.showInputDialog("What is you name?");
			while (teacher.name.equals("") || teacher.name.matches(".*\\d.*")) {
				JOptionPane.showMessageDialog(null, "Please enter a name.");
				teacher.name = JOptionPane.showInputDialog("What is you name?");
			}
			teacher.yearsTeaching = JOptionPane.showInputDialog("How many years have you been teaching?");
			while (teacher.yearsTeaching.equals("") || !teacher.yearsTeaching.matches(".*\\d.*")) {
				JOptionPane.showMessageDialog(null, "Please enter the years you have been teaching.");
				teacher.yearsTeaching = JOptionPane.showInputDialog("How many years have you been teaching?");
			}
			teacher.className = JOptionPane.showInputDialog("What class do you teach?");
			while (teacher.className.equals("") || teacher.className.matches(".*\\d.*")) {
				JOptionPane.showMessageDialog(null, "Please enter the name of the class you teach.");
				teacher.className = JOptionPane.showInputDialog("What class do you teach?");
			}
			teacher.studentsInClass = JOptionPane.showInputDialog("How many students are in your class?");
			while (teacher.studentsInClass.equals("") || !teacher.studentsInClass.matches(".*\\d.*")) {
				JOptionPane.showMessageDialog(null, "Please enter the number of students in your class.");
				teacher.className = JOptionPane.showInputDialog("What class do you teach?");
			}
			teacher.periodClass = JOptionPane.showInputDialog("What period do you teach this class?");
			while (teacher.periodClass.equals("") || !teacher.periodClass.matches(".*\\d.*")) {
				JOptionPane.showMessageDialog(null, "Please enter the period class that you teach.");
				teacher.periodClass = JOptionPane.showInputDialog("What period do you teach this class?");
			}
			String teacherInfoName = "Your name is " + teacher.name;
			String teacherInfoYearsTeaching = "You've been teaching for " + teacher.yearsTeaching + " years. ";
			String teacherInfoClassName = "You teach " + teacher.className;
			String teacherInfoStudents = "You have " + teacher.studentsInClass + " students in your class. ";
			String teacherInfoPeriod = "You teach during " + teacher.periodClass + " hour.";
			String teacherInfo = teacherInfoName + ". " + teacherInfoYearsTeaching + teacherInfoClassName + ". " + teacherInfoStudents + teacherInfoPeriod;
			JOptionPane.showMessageDialog(null, teacherInfo);
			JOptionPane.showMessageDialog(null, "Goodbye, have a nice day!");
		}
	}
}
}
