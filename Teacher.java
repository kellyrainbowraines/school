package School.src.school;

public class Teacher {
    public String name;
	public String yearsTeaching;
	public String className;
    public String studentsInClass;
    public String periodClass;

	int numYearsTeaching = 0;
	int numOfStudents = 0;
	int numPeriod = 0;

    public Teacher() {

    }

    public Teacher(String Name, int YearsTeaching, String ClassName, int NumOfStudents, int PeriodClass) {
        name = Name;
        numYearsTeaching = Integer.parseInt(yearsTeaching);
        className = ClassName;
        numOfStudents = Integer.parseInt(studentsInClass);
        numPeriod = Integer.parseInt(periodClass);
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String newName) {
		name = newName;
	}
	
	public String getYearsTeaching() {
		return yearsTeaching;
	}
	
	public void setYearsTeaching(String newYearsTeaching) {
		yearsTeaching = newYearsTeaching;
	}
	
	public String getClassName() {
		return className;
	}
	
	public void setClassName(String newClassName) {
		className = newClassName;
	}
	
	public String getNumOfStudents() {
		return studentsInClass;
	}
	
	public void setNumOfStudents(String newNumOfStudents) {
		studentsInClass = newNumOfStudents;
	}

	public String getPeriodClass() {
		return periodClass;
	}
	
	public void setPeriodClass(String newPeriodClass) {
		periodClass = newPeriodClass;
	}
}
