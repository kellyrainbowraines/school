package School.src.school;

public class Student {
	public String name;
	public String gradYear;
	public double GPA;
	public String studentID;

	public Student() {
		
	}

	public Student(String Name, String GradYear, double theGPA, String StudentID) {
		name = Name;
		gradYear = GradYear;
		GPA = theGPA;
		studentID = StudentID;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String newName) {
		name = newName;
	}
	
	public String getGradYear() {
		return gradYear;
	}
	
	public void setGradYear(String newGradYear) {
		gradYear = newGradYear;
	}
	
	public double getGPA() {
		return GPA;
	}
	
	public void setGPA(double newGPA) {
		GPA = newGPA;
	}
	
	public String getStudentID() {
		return studentID;
	}
	
	public void setStudentID(String newStudentID) {
		studentID = newStudentID;
	}
}
